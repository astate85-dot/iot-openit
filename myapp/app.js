const express = require('express')
const app = express()
const port = 8000

require('dotenv').config();

app.post('/api/temperature', (req, res) => {
    let data = '';
    req.on('data', (chunk) => {
      data += chunk;
    });
    req.on('end', () => {
        const frame = JSON.parse(data);
        const code = parseInt("0x" + frame.data.substr(0,2));
        let alert = "";
        if (frame.data.lenght >= 6) {
          alert = parseInt("0x" + frame.data.substr(0,4))
        }
        console.log("code :", code);
        console.log("frame :", frame);
        console.log("alert :", alert);
        console.log(process.env.INFLUX_URL);
      });
    // req.on('end', () => {
    //   const frame = JSON.parse(data);
    //   const code = parseInt("0x" + frame.data.substr(0,2));
    //   const value = parseInt(frame.data.substr(2,4));
    //   console.log("code :", code);
    // });
})

app.post('/api/humidity', (req, res) => {
    let data = '';
    req.on('data', (chunk) => {
      data += chunk;
    });
    req.on('end', () => {
      const frame = JSON.parse(data);
      const code = parseInt("0x" + frame.data.substr(0,2));
      let alert = "";
      if (frame.data.lenght >= 6) {
        alert = parseInt("0x" + frame.data.substr(0,4))
      }
      console.log("code :", code);
      console.log("frame :", frame);
      console.log("alert :", alert);
    });
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})